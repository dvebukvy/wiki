* новые параметры AddScheduleEvent - schedule_type
* новые параметры Posters - person_slug, type_slug, place_slug
* новое - /poster/remind/toggle/
* новое - /poster/join/toggle/
* новое - /poster/ignore/ 
* новое - /group/schedule/time/edit
* фикс описание AnnounceFiles (image - > file)
* фикс is_admin флага в group/users/