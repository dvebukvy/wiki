## /api/topics/ ##

```
#!python

class Topics(DoAuth, JSONResponseMixin, generic.ListView):
    """
    Получение интересов 1 уровня

    Returns:
        {
            'topics':
                [
                    *{'slug':'string','title':'string','division':int}
                ]
        }
    """
```
---



## /api/topics/sub/ ##

```
#!python

class SubTopics(DoAuth, JSONResponseMixin, generic.ListView):
    """
    Получение подинтересов указанного родительского интереса.
    Args:
        topic (GET): уникальный строковой идентификатор интереса
    Returns:
        {
            'topics':
                [
                    *{'slug':'string','title':'string'}
                ]
        }
    """
```
---


## /api/user/topics/root/ ##

```
#!python

class UserRootTopics(DoAuth, JSONResponseMixin, generic.ListView):
    """
    Получение интересов 1 уровня на которые подписан пользователь

    Returns:
        {
            'topics':
                [
                    *{'slug':'string','title':'string','division':int}
                ]
        }
    """
```
---


## /api/user/topics/only_sub/ ##

```
#!python

class OnlyUserSubTopics(SubTopics):
    """
    Получение подинтересов указанного родительского интереса на которые подписан пользователь
    Args:
        topic:string (GET): уникальный строковой идентификатор интереса
    Returns:
        {
            'topics':
                [
                    *{'slug':'string','title':'string'}
                ]
        }
    """
```
---


## /api/user/topics/sub/ ##

```
#!python


class UserSubTopics(SubTopics):
    """
    Получение подинтересов указанного родительского интереса, так же помечает интересы на которые подписан пользователь
    Args:
        topic:string (GET): уникальный строковой идентификатор интереса
    Returns:
        {
            'topics':
                [
                    *{'slug':'string', 'subscribed':boolean, 'title':'string'}
                ]
        }
    """
```
---


## /api/user/topics/subscribe/ ##

```
#!python

class Subscribe(DoAuth, JSONResponseMixin, generic.View):
    """
    Подписывает пользователя на интерес. Если передан интерес 1 уровня, то пользователь будет подписан на все подинтересы указанного интереса.
    Args:
        topic:string (GET): уникальный строковой идентификатор интереса
    Returns:
        {
        }
    """
```
---


## /api/user/topics/unsubscribe/ ##

```
#!python


class UnSubscribe(DoAuth, JSONResponseMixin, generic.View):
    """
    Отписывает пользователя от интереса
    Args:
        topic:string (GET): уникальный строковой идентификатор интереса
    Returns:
        {
        }
    """
```
---