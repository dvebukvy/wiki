## /posters/type_list/ ##

```
#!python

class PostersType(DoAuth, JSONResponseMixin, generic.ListView):
    """
    Возвращает текущие афиши
    Args:
        city:int (GET): id города, по умолчанию используется гео-позиция IP
        free:bool (1|0) (GET): показывать только бесплатные
        type[]:string (GET): масств типов афиш
        inc_friends: (1:0) (GET): идут ли друзья (на данный момент он не функционирует)
    Returns:
        { 'list':
            [*{
                slug: string,
                title: string,
                place: string,
                price: string,
                contacts: string,
                topics:[*{title:string,slug:string,division:int}]
                users:[*{full_name:string,photo:string]
                person: text,
                type:string,
                image:string,
                person_slug: string,
                type_slug: string,
                place_slug: string,
                start_date: date(ISO 8601),
                end_date: date(ISO 8601)
            }]
        }

    """
```
---

## /posters/ ##

```
#!python


class Posters(DoAuth, JSONResponseMixin, site_views.PostersWithFilters):
    """
    Возвращает текущие афиши
    Args:
        city:int (GET): id города, по умолчанию используется гео-позиция IP
        free:bool (1|0) (GET): показывать только бесплатные
        type[]:string (GET): масств типов афиш
    Returns:
        { 'list':
            [*{
                slug: string,
                title: string,
                place: string,
                price: string,
                person: text,
                type:string,
                image:string,
                start_date: date(ISO 8601),
                end_date: date(ISO 8601)
            }]
        }

    """
```


## /poster/join/toggle/ ##

```

!#python

class PosterToggleJoin(DoAuth, RequiredGet, site_views.UserToggleJoinPoster):
    """
    Переключение статуса "пойду/не пойду"
    """
    required = ['slug']

    def get_poster_slug(self):
        return self.request.GET.get('slug')


```

## /poster/remind/toggle/ ##

```

!#python


class PosterToggleRemind(DoAuth, RequiredGet, site_views.UserToggleRemindPoster):
    """
    Переключение статуса "напомнить/не напоминать"
    """
    required = ['slug']

    def get_poster_slug(self):
        return self.request.GET.get('slug')


```

## /poster/ignore/ ##

```
!#python
class IgnorePoster(DoAuth, CatchHttpErrors, site_views.IgnorePoster):
    """
    Игнорирование афиш. Параметры комбинируются
    Args:
        person: string (GET) - slug персоны
        type: string (GET) - slug типа
        place: string (GET) - slug места
    """
```