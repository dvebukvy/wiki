## /discounts/ ##

```
#!python


class Discounts(DoAuth, JSONResponseMixin, site_views.Discounts):
    """
    Возвращает текущие скидки
    Args:
        duration:(temp|perm|all) (GET): постоянные, временные, все.
    Returns:
        { 'list':
            [*{
                slug: string,
                title: string,
                place: string,
                discount: string,
                content: text,
                image:string,
                start_date: date(ISO 8601),
                end_date: date(ISO 8601)
            }]
        }

    """
```