# ALERT #

##Пояснение к ебанутой системе расписания.

Ебанутая она искл. потому что нихуя не продумана и сверстана/нарисована на отьебись.

Собственно, у нас есть примерно 10 обьектов именумых "Время", если точней то это пары

1 пара 10:20 - 11:30
2 пара 11:40 - 11:50

Так же у нас есть собственно сами предметы. Которые связаны с обьектом "Время".
Стоит заметить, что, блять, связаны они через один ко многим (время к предмету)
Т.е если ты меняешь время, ТО ОНО, БЛЯТЬ, МЕНЯЕТСЯ ДЛЯ ВСЕХ ПРЕДМЕТОВ связанных с этим временем...
В качестве оправдашки - на все аргументы мол ебанулись? Ответ - времени нету, виноваты все кроме меня.






## /api/educations/ ##
```
#!python
class Educations(DoAuth, RequiredGet, edu_views.Educations):
    """
    Возвращает список образовательных учереждений
    Args:
        city_id:int (GET): id города

    Returns:
        { 'educations':
            [*{
                pk: int,
                title: string,
            }]
        }

    """
```

## /groups/ ##
```
#!python

class Groups(DoAuth, RequiredGet, edu_views.Groups):
    """
    Возвращает список групп
    Args:
        education_id:int (GET): id уч. заведения

    Returns:
        { 'groups':
            [*{
                pk: int,
                title: string,
                slug: string,
            }]
        }

    """
```


## /group/users/ ##
```
#!python

class GroupUsers(DoAuth, RequiredGet, JSONResponseMixin, generic.ListView):
    """
    Возвращает список пользователей в группе

    Статусы юзеров:
        IN_QUEUE = 1
        REJECTED = 2
        ACCEPTED = 3

    Args:
        group_slug:string (GET): slug группы
        status: int(GET): вернуть только пользователей со статусом, по умолчанию ACCEPTED (3)

    Returns:
        { 'users':
            [*{
                pk: int,
                full_name: string,
                photo: string,
                is_admin: boolean,
                status:IN_QUEUE|REJECTED|ACCEPTED
            }]
        }

    """
```


## /group/create/ ##
```
#!python


class CreateGroup(CSRFExempt, DoAuth, JSONResponseMixin, edu_views.CreateGroup):
    """
    Создание новой группы. Стоит заметить, что параметры передаются POST запросом.

    Args:
        education: int (POST): id уч. заведения
        title: string (POST): название

    Returns:
        {pk: int}

    ReturnsIfError:
        {**field_name|string: *errors:string}
        Example:
            {
            "errors":
                "education": [
                    "Обязательное поле."
                ],
                "title": [
                    "Обязательное поле."
                ],
                "__all__": [
                    "Пользователь уже находится в группе"
                ]
            }
    """
```


## /group/leave/ ##
```
#!python

class LeaveFromGroup(DoAuth, RequiredGet, edu_views.LeaveFromGroup):
    """
    Выход юзера из группы.
    Если админн и группа не пуста - начнется голосование
    Если не админ - тупо ливнет

    Returns: {
            'status':int(1|2|3) (Удалена потому что пуста|Тупо ливнул|Админ, началось голосование)
            'event_slug':string - slug голосовалки если status == 3
        }

    """

```


## /group/join/ ##
```
#!python

class GroupGetInvite(CatchHttpErrors, DoAuth, RequiredGet, edu_views.GroupGetInvite):
    """
    Вступить в группу
    Args:
        group_slug:string (GET): slug группы
    """
```



## /group/admin/accept/ ##
```
#!python

class AcceptUser(UserDestiny, edu_views.GroupOwnerAcceptUser):
    """
    Ацепает юзера в группу
    Args:
        group_slug:string (GET): slug группы
        user_id:id (GET)
    Returns:
        { 'status': 'success'}

    """

```


##  /group/admin/reject/ ##
```
#!python


class RejectUser(UserDestiny, edu_views.GroupOwnerRejectUser):
    """
    Посылает нахер юзера из очереди в группу
    Args:
        group_slug:string (GET): slug группы
        user_id:id (GET)
    Returns:
        { 'status': 'success'}

    """

```


## /group/admin/change_owner/ ##
```
#!python


class ChangeGroupOwner(CatchHttpErrors, DoAuth, edu_views.ChangeGroupOwner):
    """
    Меняет админа в группе
    Args:
        group_slug:string (GET): slug группы
        user_id:id (GET)
    Returns:
        { 'status': 'success'}

    """

```


## /group/admin/delete/ ##
```
#!python

class DeleteGroup(CatchHttpErrors, DoAuth, edu_views.DeleteGroup):
    """
    Удаляет группу
    Args:
        group_slug:string (GET): slug группы
    Returns:
        { 'status': 'success'}

    """

```



## /group/admin/remove_user/ ##
```
#!python


class RemoveUserFromGroup(CatchHttpErrors, DoAuth, edu_views.RemoveUserFromGroupHandle):
    """
    Удалить юзера из группы
    Args:
        user_id:int (GET)
        group_slug:string (GET):
    Returns: {}

    """

```



## /group/event/create/owner/ ##
```
#!python

class MakeNewOwnerVote(CatchHttpErrors, DoAuth, edu_views.HandleGroupOwnerVote):
    """
    Создает голосование на смену админа группы
    Args:
        group_slug:string (GET): slug группы
    Returns:
        { 'slug': 'slug события'}

    """

```



## /group/event/create/kick/ ##
```
#!python
class MakeNewKickUserVote(CatchHttpErrors, DoAuth, edu_views.KickUserVoteHandle):
    """
    начать голосование за искл. юзера из группы
    Args:
        user_id:int (GET)
        group_slug:string (GET):
    Returns: {}

    """

```



## /group/event/results/ ##
```
#!python

class VoteResults(CatchHttpErrors, DoAuth, edu_views.VoteResults):
    """
    Инфа о результатах голосования. Можно юзать. чтобы получить список вариантов
    Args:
        event_slug:string (GET): slug события
    Returns:
        { 'votes': *[pk: int, title: string, vote_count: int]}

    """

```



## /group/event/vote// ##
```
#!python

class Vote(CatchHttpErrors, DoAuth, edu_views.VoteVariant):
    """
    Голосовать за
    Args:
        event_slug:string (GET): slug события
        variant_pk:int (GET): id варианта
    Returns:
        { 'votes': *[pk: int, title: string, vote_count: int]}

    """

```




## /group/event/create/vote/ ##
```
#!python


class CreateVote(CatchHttpErrors, CSRFExempt, DoAuth, RequiredGet, edu_views.CreateVote):
    """
    Создать голосование
    Args:
        group_slug:string (GET): slug группы

        title:string(POST): название голосования
        *variants[]:*strings(POST): варианты ответов
    Returns:
        { 'slug': string}

    """

```




## /group/event/create/photo/ ##
```
#!python

class CreatePhoto(CatchHttpErrors, CSRFExempt, DoAuth, RequiredGet, edu_views.CreatePhoto):
    """
    Создать голосование
    Args:
        group_slug:string (GET): slug группы

        title:string(POST): название голосования

        *files[]:*files(POST):
        *file_title[]:*string(POST):
        *file_description[]:*string(POST):

    Returns:
        { 'slug': string}

    """

```




## /group/event/create/announce/ ##
```
#!python


class CreateAnnounce(CatchHttpErrors, CSRFExempt, DoAuth, RequiredGet, FileHandle, edu_views.CreateAnnounce):
    """
    Создать обьявление
    Args:
        group_slug:string (GET): slug группы

        title:string(POST): название голосования
        description:string(POST)
        date:string(POST): дата, формат - 01.01.2012 — 02.03.2015 (д.м.г) или 01.01.2012

        *files[]:*files(POST):
        *file_title[]:*string(POST):
        *file_description[]:*string(POST):

    Returns:
        { 'slug': string}

    """

```




## /group/event/create/schedule/ ##
```
#!python


class AddScheduleEvent(CatchHttpErrors, CSRFExempt, DoAuth, RequiredGet, edu_views.ScheduleAddHandle):
    """
    Создать предмет
    Args:
        group_slug:string (GET): slug группы

        title:string(POST)
        audience:string(POST)
        description:string(POST)
        date:string(POST): дата, 2014.11.29
        time_object:int(POST): id пары/времени
        schedule_type: int(POST): id типа события
    Returns:
        { 'slug': string}

    """

```




## /events/ ##
```
#!python


class Events(CatchHttpErrors, DoAuth, edu_views.Events):
    """
        Все события
        Args:
            date:%Y.%m фильтр по месяцу
            
        Returns:
            см. class Event() (ниже)
    """

```


## /event/ ##
```
#!python


class Event(CatchHttpErrors, RequiredGet, DoAuth, JSONResponseMixin, edu_views.Event):
    """
    
    Returns:
        Данные которые всегда возвращаются к любому типу события
            title: string
            creator_id: int
            creator_name: string
            created: datetime
            slug: string
            group_slug: string

        Для голосования (любого)
            votes: *[pk: int, title: string, vote_count: int)
            completed: boolean
            is_voted: boolean
            type:
                vote - обычное голосование
                owner_vote - голосование на смену админа
                kick_user - голосование на кик юзера

        Фото событие
            images: *[title: string, description: string, image: string]
            type: photo

        Обьявление/Анонс/Файлики
            files: *[title: string, description: string, image: string]
            description: string
            date_start: date iso
            date_stop: date iso
            time_start: time 22:59
            time_stop: time 22:59
            type: announce
    """


```


## /group/schedule/time_list/ ##
```
#!python

class ScheduleTimeList(CatchHttpErrors, DoAuth, RequiredGet, JSONResponseMixin, edu_views.GroupPage, generic.View):
    """
    Список пар для группы
    Args:
        group_slug:string (GET): slug группы
    """

```



## /schedule ##

```
#!python
class Schedule(DoAuth, RequiredGet, JSONResponseMixin, edu_views.ScheduleEvent):
    """
    {
        'title': string,
        'type_title': тип
        'audience': аудитория,
        'group': название группы,
        'group_slug': слаг группы,
        'time': {
            'pk': айдишник времени,
            'start': начало,
            'stop': конец,
            'pos': номер пара,
        },
        'description': описание,
        'creator': {
            'pk': айди юзера,
            'photo': фото юзера,
            'name': имя юзера,
        },
        'description': описание,
        'date': дата Год.Месяц.День,
        'slug': слаг,
        'sub_events':*[see class Event]
    }
    """
```

## /schedules/ ##
```
#!python

class Schedules(DoAuth, RequiredGet, JSONResponseMixin, edu_views.Schedule):
    """
    Args:
        start: %Y.%m.%d
    Returns:
        Год.Месяц.День {
            schedule: {
                'title': string,
                'type_title': тип
                'audience': аудитория,
                'group': название группы,
                'group_slug': слаг группы,
                'time': {
                    'pk': айдишник времени,
                    'start': начало,
                    'stop': конец,
                    'pos': номер пара,
                },
                'description': описание,
                'creator': {
                    'pk': айди юзера,
                    'photo': фото юзера,
                    'name': имя юзера,
                },
                'description': описание,
                'date': дата Год.Месяц.День,
                'slug': слаг,
            }
        """
```

## /schedules/types/ ##

```
#!python

class ScheduleItemTypes(DoAuth, JSONResponseMixin, generic.ListView):
    """
    Возвращает типы занятий
    Returns
        *{
            id: int,
            title: string
        }
    """
```


## /group/schedule/time/edit##

```
!#python

class ScheduleEditTime(CatchHttpErrors, DoAuth, RequiredGet, edu_views.ScheduleChangeTime):
    """
    Изменение времени
    """
    
```