## /cities/ ##

```
#!python

class Cities(DoAuth, JSONResponseMixin, generic.ListView):
    """
    Возвращает список городов
    Returns:
        { 'list':
            [*{pk:int,name:string}]
        }

    """
```
---

## /posters/type_list/ ##

```
#!python

class PostersType(DoAuth, JSONResponseMixin, generic.ListView):
    """
    Возвращает типы афиш
    Returns:
        { 'list':
            [*{slug:string,title:string}]
        }

    """
```
---

## /posters/ ##

```
#!python


class Posters(DoAuth, JSONResponseMixin, site_views.PostersWithFilters):
    """
    Возвращает текущие афиши
    Args:
        city:int (GET): id города, по умолчанию используется гео-позиция IP
        free:bool (1|0) (GET): показывать только бесплатные
        type[]:string (GET): масств типов афиш
    Returns:
        { 'list':
            [*{
                slug: string,
                title: string,
                place: string,
                price: string,
                person: text,
                type:string,
                image:string,
                start_date: date(ISO 8601),
                end_date: date(ISO 8601)
            }]
        }

    """
```
---
## /discounts/ ##

```
#!python


class Discounts(DoAuth, JSONResponseMixin, site_views.Discounts):
    """
    Возвращает текущие скидки
    Returns:
        { 'list':
            [*{
                slug: string,
                title: string,
                place: string,
                discount: string,
                content: text,
                image:string,
                start_date: date(ISO 8601),
                end_date: date(ISO 8601)
            }]
        }

    """
```