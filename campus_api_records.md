## /api/records/topic/ ##

```
#!python


class RecordsByTopic(DoAuth, JSONResponseMixin, generic.ListView):
    """
    Получить материалы по интересу 1 уровня
    Args:
        topic:string (GET): уникальный строковой идентификатор интереса
        limit:int (GET): кол-во записей. По умолчанию  10
        prev_records:string (GET): идентификаторы записей разделенные запятой из предыдущего запроса.
    Returns:
        {
            'records':
                [
                    *{'slug':'record_id:string','title':'string','url':'string','title':'string','summary':'multiline','image':'string'}
                ]
        }
    """
```
---

## /api/user/record/like/ ##
```
#!python
class Like(DoAuth, JSONResponseMixin, generic.View):
    """
    Лайк материала пользователем
    Args:
        record:string (GET): уникальный строковой идентификатор материала (slug)
    Returns:
        {
        }
    """
```
---


## /api/user/record/dislike/ ##
```
#!python
class Dislike(DoAuth, JSONResponseMixin, generic.View):
    """
    Дизлайк материала пользователем
    Args:
        record:string (GET): уникальный строковой идентификатор материала (slug)
    Returns:
        {
        }
    """
```
---

## Общее для лайков и дизлайков ##
```
#!python
class LikeDislikeContextMixin(DoAuth, JSONResponseMixin):
    """
    Лайки/дизлайки пользователя
    Args:
        topic:string (GET): интерес
        date:date (GET): ГодМесяц. Ex: 201404
    Returns:
        { 'records':
            *[{
                    'topic': {'title':'string', 'division': int},
                    'record': {'slug': 'string', 'title': 'string', 'url': 'string', 'image': 'string|false'}
             }]
        }
    """


```
## /api/user/record/like_list/ ##

```
#!python
class LikeList(LikeDislikeContextMixin, site_views.LikeList):
    pass
```
---

## /api/user/record/dislike_list/ ##

```
#!python
class DislikeList(LikeDislikeContextMixin, site_views.DislikeList):
    pass
```
---