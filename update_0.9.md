## /api/schedules/types/ ##

```
#!python

class ScheduleItemTypes(DoAuth, JSONResponseMixin, generic.ListView):
    """
    Возвращает типы занятий
    Returns
        *{
            id: int,
            title: string
        }
    """
```
## MainPage
rebuild:int (GET): если == 1, то главная страница будет перестроена полностью


## Обновление GroupUsers
```
#!python

class GroupUsers(DoAuth, RequiredGet, JSONResponseMixin, generic.ListView):
    """
    Возвращает список пользователей в группе

    Статусы юзеров:
        IN_QUEUE = 1
        REJECTED = 2
        ACCEPTED = 3

    Args:
        group_slug:string (GET): slug группы
        status: int(GET): вернуть только пользователей со статусом, по умолчанию ACCEPTED (3)

    Returns:
        { 'users':
            [*{
                pk: int,
                full_name: string,
                photo: string,
                is_admin: boolean,
                status:IN_QUEUE|REJECTED|ACCEPTED
            }]
        }

    """
```