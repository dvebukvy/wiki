```
#!python
class Posters(DoAuth, JSONResponseMixin, site_views.PostersWithFilters):
  """
    Возвращает текущие афиши
    Args:
        city:int (GET): id города, по умолчанию используется гео-позиция IP
        free:bool (1|0) (GET): показывать только бесплатные
        type[]:string (GET): масств типов афиш
    Returns:
        { 'list':
            [*{
                slug: string,
                title: string,
                place: string,
                price: string,
                contacts: string,
                person: text,
                type:string,
                image:string,
                start_date: date(ISO 8601),
                end_date: date(ISO 8601),
                topics:[*{title:string,slug:string,division:int}],
                users:[*{full_name:string,photo:string]
            }]
        }

    """
```
### New: Returns
* ``` contacts: string```
* ```topics:[*{title:string,slug:string,division:int}]```
* ``` users:[*{full_name:string,photo:string]```
---
---

```
#!python
class Discounts(DoAuth, JSONResponseMixin, site_views.Discounts):
    """
    Возвращает текущие скидки
    Args:
        duration:(temp|perm|all) (GET): постоянные, временные, все.
```
### New: Args:
*```duration:(temp|perm|all) (GET): постоянные, временные, все.```