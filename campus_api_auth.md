## /api/auth/ ##

```
#!python

class Auth(JSONResponseMixin, generic.View):
    """
    Авторизация, получение ключа
    Args:
        provider:string (GET): соц. сеть (facebook,vkonakte,twitter)
        token:string (GET): токен выданный соц. сетью
    Returns:
        {
            'token':'string',
            'social_token':'string',
            'is_new':boolean
        }
    """
```
---

## /auth/registration/native/ ##
```
#!python
class NativeRegistration(CSRFExempt, JSONResponseMixin, generic.FormView):
    """
    Args:
        email: string - логин он же email
        first_name: string - юзернейм
        password:string - пароль
        education_id:int - ID учебки
    Returns:
        {'token':string}
    Если форма не валидна, то:
    Returns :
        { 'errors':
            [*{
                error_field_name:[msg,msg,msg..],
            }]
        }

    """
```
---
## /auth/login/native/ ##

```
#!python

class NativeLogin(CSRFExempt, JSONResponseMixin, generic.FormView):
    """
    Логин через email / password
    Args:
        username:string (POST) - логин он же email
        password:string (POST) - пароль
    Returns:
        {'token':string}
    Если форма не валидна, то:
    Returns :
        { 'errors':
            [*{
                error_field_name:[msg,msg,msg..],
            }]
        }
    """
```
---
## /auth/reset ##

```
#!python

class ResetPassword(site_views.ResetPassword):
    """
   Args:
       email:string - логин он же email
   Returns:
       {}
   """
```

## /profile ##
```
#!python


class Profile(CSRFExempt, DoAuth, site_views.Profile):
    """
    Профиль юзера
    GET:
        Returns:
            {
                "city_pk": int,
                "city": string,
                "group_slug": string (slug),
                "group": string,
                "photo": string,
                "is_admin": boolean,
                "full_name": string,
                "pk": int,
                "education_pk": int,
                "education": string,
                "email": string
            }

    POST:
        Args:
            first_name*: string,
            sex*: int(0|1|2),
            photo: FILE OBJECT

            password1: string
            password2: string
        """

```