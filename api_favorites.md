## /api/user/records/favorites/ ##
```
#!python

class UserFavorites(DoAuth, JSONResponseMixin, generic.ListView):
    """
    Избранное пользователя
    Args:
        record:string (GET): уникальный строковой идентификатор материала (slug)
        topic[0]:string (GET): масств уникальных строковых идентификаторов интересов
        start:timestamp (GET): фильтр - начальная дата
        end:timestamp (GET): фильтр - конечная дата
        timezone:string (GET)
    Returns:
        { 'records':
            *[{'topic': {'title':'string', 'division': int},
                    'record': {'slug': 'string', 'title': 'string', 'url': 'string', 'image': 'string|false'}
             }]
        }
    """
```



## /api/user/record/add_favorite/ ##

```
#!python
class ToFavorites(DoAuth, JSONResponseMixin, generic.View):
    """
    Добавление материала в избранное
    Args:
        record:string (GET): уникальный строковой идентификатор материала (slug)
    Returns:
        {
        }
    """

```
---


## /api/user/record/remove_favorite/ ##
```
#!python
class RemoveFromFavorites(DoAuth, JSONResponseMixin, generic.View):
    """
    Удаление материала из избранного
    Args:
        record:string (GET): уникальный строковой идентификатор материала (slug)
    Returns:
        {
        }
    """
```