## /cities/ ##

```
#!python

class Cities(JSONResponseMixin, generic.ListView):
    """
    Возвращает список городов
    Returns:
        { 'list':
            [*{pk:int,name:string}]
        }

    """
```

---


## /educations/ ##

```
#!python
class Educations(JSONResponseMixin, generic.ListView):
    """
    Возвращает список образовательных учереждений
    Args:
        city:int (GET): id города, по умолчанию вернет все заведения

    Returns:
        { 'educations':
            [*{
                pk: int,
                title: string,
            }]
        }

    """
```
---

## /main_page ##

```
#!python
class MainPage(DoAuth, JSONResponseMixin, generic.View):
    """
    Предустановленные материалы для главной
  
    Args:
        rebuild:int (GET): если == 1, то главная страница будет перестроена полностью.
    Returns:
        {
            'records':
                [

                    *{'slug':'record_id:string','title':'string','url':'string','title':'string','summary':'multiline','image':'string','topic':string,division:int}
                ]
        }
    """
```
---