## /educations/ ##

```
#!python
class Educations(JSONResponseMixin, generic.ListView):
    """
    Возвращает список образовательных учереждений
    Args:
        city:int (GET): id города, по умолчанию вернет все заведения

    Returns:
        { 'educations':
            [*{
                pk: int,
                title: string,
            }]
        }

    """
```

---

## /auth/registration/native/ ##
```
#!python
class NativeRegistration(CSRFExempt, JSONResponseMixin, generic.FormView):
    """

    Args:
        email:string - логин он же email
        password:string - пароль
        education_id:int - ID учебки
    Returns:
        {'token':string}
    Если форма не валидна, то:
    Returns :
        { 'errors':
            [*{
                error_field_name:[msg,msg,msg..],
            }]
        }

    """
```
---
## /auth/login/native/ ##

```
#!python

class NativeLogin(CSRFExempt, JSONResponseMixin, generic.FormView):
    """
    Логин через email / password
    Args:
        username:string (POST) - логин он же email
        password:string (POST) - пароль
    Returns:
        {'token':string}
    Если форма не валидна, то:
    Returns :
        { 'errors':
            [*{
                error_field_name:[msg,msg,msg..],
            }]
        }
    """
```
